/** \brief The colormap to use for the different type of events. */
let color_map = {
 "death"          : "#ffeda0",
 "Hall_d_entree"  : "#84B1D8",
 "IOA"            : "#EE8373",
 "Boxes"          : "#B9DC7C",
 "Zone_1"         : "#D9D9D9",
 "Ambu"           : "#F4B862",
 "ZSA"            : "#D9302C",
 "Zone_2"         : "#999999",
 "Dechocage"      : "#B582BB",
 "unknown0"       : "#A0562F",
 "interruption"   : "#555555",
 "default"        : "lightgray",
};

/** \brief In addition to color_map, this object lists, for all type of event, whether the background (color_map) is a dark color */
let is_color_dark = {
  "death"        : false,
  "Hall_d_entree": false,
  "IOA"          : true,
  "Boxes"        : false,
  "Zone_1"       : false,
  "Ambu"         : false,
  "ZSA"          : true,
  "Zone_2"       : true,
  "Dechocage"    : true,
  "unknown0"     : true,
  "interruption" : true,
  "default"      : false,
};

export { color_map, is_color_dark };
