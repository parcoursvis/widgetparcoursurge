/** \brief  The main views the app supports*/
const ViewType = {
  MAIN_VIEW_TYPE:     0, /*!< Main Tree view*/
  SEQUENCE_VIEW_TYPE: 1, /*!< The view that is aligned by a sequence (before/after)*/
  END_VIEW_TYPE:      2,
};

/** \brief  The different tab this application supports (right part of the screen) */
const TabView = {
  OVERVIEW_TAB: "OVERVIEW_TAB", /*!< Display global information*/
  DETAIL_TAB  : "DETAIL_TAB",   /*!< Display details about a node*/
  CONTROL_TAB : "CONTROL_TAB",  /*!< Handle filtering parameters*/
  HISTORY_TAB : "HISTORY_TAB"   /*!< Display a tree of past events*/
};

/** \brief The different modes for comparisons */
const ComparisonMode = {
  SOURCE_COMPARISON_MODE : 0, /*!< The user is comparing two dataset and visualizing the source state*/
  TARGET_COMPARISON_MODE : 1, /*!< The user is comparing two dataset and visualizing the target state*/
  NONE_COMPARISON_MODE   : 2  /*!< No comparison for the moment*/
};

/** \brief  In the SequenceView, what is the tree the user is visualizing? */
const TreeToDisplay = {
  BEFORE: 0, /*!< Visualizing past events*/
  AFTER:  1, /*!< Visualizing to-come events*/
};

/** \brief  Which types can be discarded? */
const DiscardEntries = ["dual_sequence", "after", "before"];

/** \brief  The different "root" nodes a tree can have (for instance, a tree can be composed of two sub-trees, each that should be considered separately) */
const RootEntries    = ["root", "before", "after"];

export { ViewType, TabView, ComparisonMode, TreeToDisplay, DiscardEntries, RootEntries };
