import { DOMWidgetModel, DOMWidgetView } from '@jupyter-widgets/base';
import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import Store from './plugins/vuex';
import { ViewType } from './Config';
import 'roboto-fontface/css/roboto/roboto-fontface.css';

// See example.py for the kernel counterpart to this file.

// Custom Model. Custom widgets models must at least provide default values
// for model attributes, including
//
//  - `_view_name`
//  - `_view_module`
//  - `_view_module_version`
//
//  - `_model_name`
//  - `_model_module`
//  - `_model_module_version`
//
//  when different from the base class.

// When serialiazing the entire widget state for embedding, only values that
// differ from the defaults will be serialized.

export class AppModel extends DOMWidgetModel {
  defaults() {
    return {
      ...super.defaults(),
      _model_name : 'AppModel',
      _view_name : 'AppView',
      _model_module : 'jsParcoursUrge',
      _view_module : 'jsParcoursUrge',
      _model_module_version : '0.1.0',
      _view_module_version : '0.1.0',
      current_sequence: [],
      sequence_view_zoomed_nodes_ids : [-1,-1],
      main_view_zoomed_node_id : -1,
      view_type : ViewType.MAIN_VIEW_TYPE,
    };
  }
}

export class AppView extends DOMWidgetView {
  render() {
    //Increase the container's width / reduce its margin
    //Note that THIS IS A HACK, normally we should not do that...
    var container = document.getElementById("notebook-container");
    if(container != null) { //we are in the regular notebook 
      container.style.padding  = "";
      container.style.maxWidth = "100%";
      container.style.margin   = "";
    }

    //Create a div on which to link the main Vue component with it.
    var appDiv = document.createElement('div');
    appDiv.id = "app";
    this.el.appendChild(appDiv);

    //The issue is that the App div may not be added to the DOM yet.
    //We thus wait for it to be added
    var waitApp = function(callback) {
      if(document.getElementById("app") != null) {
        callback();
        return;
      }
      else
        setTimeout(function() { 
          waitApp(callback);
        }, 50);
    };

    var store = new Store((msg) => {
      this.send({'data': msg});
    }, this.model);
    this.store = store;

    waitApp(() => {
      new Vue({
        vuetify,
        store,
        render: h => h(App)
      }).$mount('#app');
    });

    //Observe and act on future changes to the value attribute
    this.model.on('msg:custom', this.on_msg, this);
  }

  on_msg(msg, _buffers) {
    this.store.push_msg(msg.data);
  }
}
