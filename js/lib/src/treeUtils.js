/** \brief  Get the list of the parents of a node.
  * \param tree the root of the tree to evaluate
  * \param node the node to find the parents of
  * \return   The list of the parents. Empty if no parents is found. lower index ==> "closer" parents*/
function getParents(tree, node) {
  let parentsRec = (curnode, node, _parents) => {
    if(curnode == node)
      return _parents;

    let parents = [..._parents, curnode];
    for(let i = 0; i < curnode.children.length; i++) {
      let res = parentsRec(curnode.children[i], node, parents);
      if(res.length > 0)
        return res;
    }
    return [];
  };
  return parentsRec(tree, node, []);
}

/** \brief  Search a node in a tree by its id. This function performs an in-depth search
  * \param d the root of the tree
  * \param id the ID of the node to search for
  * \return  The node if found, null otherwise */
function searchNodeByID(d, id) {
  if(d == null)
    return null;
  if(d.id == id)
    return d;
  for(let i = 0; i < d.children.length; i++)
  {
    let res = searchNodeByID(d.children[i], id);
    if(res != null)
      return res;
  }
  return null;
}

function hysteresisSortDesc(oldNode, newNode, inertia=0.02) {
  //No tree before? Sort perfectly the tree
  if(oldNode == null) {
    newNode.children.sort((a, b) => b.count - a.count);
    return newNode;
  }

  //Check that newNode and oldNode are related
  if(newNode.id != oldNode.id || 
    newNode.count < oldNode.count ||
    newNode.children.length < oldNode.children.length) {
    console.warn("Cannot apply an hysteresis on non-continuous update. Revert to default sorting");
    newNode.children.sort((a, b) => b.count - a.count);
    return newNode;
  }

  //Put new children AT THE END of the list
  for(let i = 0; i < oldNode.children.length; i++) {
    if(oldNode.children[i].id != newNode.children[i].id) { //The order has changed --> resort
      let found = false;
      for(let j = i; j < newNode.children.length; j++) {
        if(newNode.children[j].id == oldNode.children[i].id) { //Found the correct child at position j --> Swap
          let temp = newNode.children[j];
          newNode.children[j] = newNode.children[i];
          newNode.children[i] = temp;
          found = true;
          break;
        }
      }
      if(!found) {
        console.warn("Cannot apply an hysteresis on non-continuous update. Revert to default sorting");
        newNode.children.sort((a, b) => b.count - a.count);
        return newNode;
      }
    }
  }

  //And check the sorting with an inertia using a bubble sort
  //This "imperfect" sorting ensures the following equation: 
  //forall{i, j} : 0 < i < j < len(sortedNodes) -> weight(sortedNodes[j]) − weight(sortedNodes[i]) ≤ hysteresisInertia
  if(newNode.children.length > 1) {
    let hasPermuted = true;
    while(hasPermuted) {
      hasPermuted     = false;
      let minValue        = newNode.children[0].count;
      let lastMinValueIDx = 0;
      for(let i = 1; i < newNode.children.length; i++) { //We cannot optimize the start index because of the inertia condition
        if(minValue < newNode.children[i].count && (newNode.children[i].count-minValue)/newNode.count > inertia) { //Hysteresis condition not satisfied: A bubble is detected. Continuous swap
          for(let j = i; j > lastMinValueIDx; j--) {
            let temp = newNode.children[j-1];
            newNode.children[j-1] = newNode.children[j];
            newNode.children[j]   = temp;
          }
          lastMinValueIDx++;
          hasPermuted = true;
        }
        else if(minValue >= newNode.children[i].count) { //>= ensures the minimum number of permutations
          minValue = newNode.children[i].count;
          lastMinValueIDx = i;
        }
      }
    }
  }

  return newNode;
}

function hysteresisSortAsc(oldNode, newNode, inertia=0.02) {
  //No tree before? Sort perfectly the tree
  if(oldNode == null) {
    newNode.children.sort((a, b) => a.count - b.count);
    return newNode;
  }

  //Check that newNode and oldNode are related
  if(newNode.id != oldNode.id || 
    newNode.count < oldNode.count ||
    newNode.children.length < oldNode.children.length) {
    console.warn("Cannot apply an hysteresis on non-continuous update. Revert to default sorting");
    newNode.children.sort((a, b) => a.count - b.count);
    return newNode;
  }

  //Put new children AT THE BEGINNING of the list
  let delta = newNode.children.length - oldNode.children.length;
  for(let i = 0; i < oldNode.children.length; i++) {
    if(oldNode.children[i].id != newNode.children[i+delta].id) { //The order has changed --> resort
      let found = false;
      for(let j = 0; j < newNode.children.length; j++) {
        if(newNode.children[j].id == oldNode.children[i].id) { //Found the correct child at position j --> Swap
          let temp = newNode.children[j];
          newNode.children[j]       = newNode.children[i+delta];
          newNode.children[i+delta] = temp;
          found = true;
          break;
        }
      }
      if(!found) {
        console.warn("Cannot apply an hysteresis on non-continuous update. Revert to default sorting");
        newNode.children.sort((a, b) => a.count - b.count);
        return newNode;
      }
    }
  }

  //And check the sorting with an inertia using a bubble sort
  //This "imperfect" sorting ensures the following equation: 
  //forall{i, j} : 0 < i < j < len(sortedNodes) -> weight(sortedNodes[i]) − weight(sortedNodes[j]) <= hysteresisInertia
  if(newNode.children.length > 1) {
    let hasPermuted = true;
    while(hasPermuted) {
      hasPermuted        = false;
      let maxValue       = newNode.children[0].count;
      let lastMaxValueID = 0;
      for(let i = 1; i < newNode.children.length; i++) { //We cannot optimize the start index because of the inertia condition
        if(maxValue > newNode.children[i].count && (maxValue-newNode.children[i].count)/newNode.count > inertia) { //Hysteresis condition not satisfied: A bubble is detected. Continuous swap
          for(let j = i; j > lastMaxValueID; j--) {
            let temp = newNode.children[j-1];
            newNode.children[j-1] = newNode.children[j];
            newNode.children[j]   = temp;
          }
          lastMaxValueID++;
          hasPermuted = true;
        }
        else if(maxValue <= newNode.children[i].count) { // <= ensures the maximum number of permutations
          maxValue = newNode.children[i].count;
          lastMaxValueID = i;
        }
      }
    }
  }

  return newNode;
}

export { getParents, searchNodeByID, hysteresisSortAsc, hysteresisSortDesc };
