const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');

const rules = [
  {
    test: /\.s(c|a)ss$/,
    use: [
      'vue-style-loader',
      'css-loader',
      {
        loader: 'sass-loader',
      },
    ],
  },
  { test: /\.vue$/, loader: 'vue-loader', options: { optimizeSSR: false } },
  { test: /\.js$/, loader: 'babel-loader'},
];


module.exports = {
  module: {rules: rules},
  plugins: [
    new VueLoaderPlugin(),
    new VuetifyLoaderPlugin(),
  ],
  resolve: {
    extensions: [ '.js', '.vue' ],
    alias: {
      '@': path.resolve(__dirname, 'lib/src')
    }
  }
};
