# ParcoursUrge

ParcoursURGE is an adaptation of [[ParcoursVis]](https://gitlab.inria.fr/aviz/parcoursvis) for the URGE project. It supports the visual analysis of patients' care pathways at emergency facilities from AP-HP hospitals.

This project is an adaptation of [[ParcoursURGE]](https://gitlab.inria.fr/parcoursvis/parcoursurge) to work within the Jupyter Notebook and Jupyter Lab environments.

It allows, using Jupyter python environment, the loading of a supported dataset (see [the preparser of parcoursurge](https://gitlab.inria.fr/parcoursvis/parcoursurge/-/tree/master/back/parser) and the [convert\_from\_simulation.py](https://gitlab.inria.fr/parcoursvis/parcoursurge/-/blob/master/back/generate_data/convert_from_simulation.py) python script) and its visualization using the ParcoursVis interactive tool.

The generic python code is the following:

```
from pyParcoursUrge import ParcoursUrge
data_dir="path/to/data" #The relative (from the Jupyter point of view) or absolute path of the dataset.
v = ParcoursUrge(data_dir)
v #Display the Widget
```

## Git submodules
first initialize and fetch the submodules:
```$ git submodule update --init --recursive```

### conda installation (recommanded)
With a linux system:
```
$ cd parcoursurge/back
$ conda env create -f environment.yml
$ cd - #go back to the root directory
```

with a Mac OSX system:
```
$ cd parcoursurge/back
$ conda env create -f environment-osx.yml
$ cd - #go back to the root directory
```

This will create the conda environment "parcoursurge", on which jupyter notebook should be installed:
```
$ conda activate parcoursurge #use parcoursurge conda environment
$ conda install -c conda-forge jupyter
```

For JupyterLab, you will also need the following dependencies:
```
$ conda install -c conda-forge jupyterlab jupyter-packaging
```
    
### Non conda environments

The dependencies of WidgetParcoursUrge are:
- python>=3.10.4
- g++
- make
- nodejs
- pip
- jupyter notebook
- jupyterlab
- jupyter-packaging

## Installation

You must then install the Widget module as follow in the conda environment 'parcoursurge' you just created or any other environment that installed the cited dependencies.

### Pip installation
```$ pip install .```

### Development installation

- Install jupyter\_packaging
```$ conda install -c conda-forge jupyter-packaging```

- Install Python dependencies
```$ pip  install -e .```

- Register the jupyter lab/nb extensions
```
$ jupyter nbextension install --py --symlink --overwrite --sys-prefix pyParcoursUrge
$ jupyter nbextension enable --py --sys-prefix pyParcoursUrge
$ jupyter labextension develop --overwrite .
```

- Each time you need to rebuild the JS frontend sourcecode because of modifications:
```
$ cd js
$ npm run build
```
You then need to refresh the JupyterLab page.

## Environments without Node and C/C++ compilers using Docker images
To install those plugins without the use of Node and C/C++ compilers, you need to:
- Follow instructions on https://gitlab.inria.fr/parcoursvis/parcoursurge/-/tree/master/back to compile the C/C++--Python binding module using a dedicated Docker image
- Install manually the resulted .so (e.g., parcoursprog.cpython-310-x86_64-linux-gnu.so) and parcoursprog.egg-info or set the PYTHONPATH environment variable.
- Create the JS toolchain Docker image and launch it as follow:
```
cd js/
docker build -f toolchain.Dockerfile -t widgetparcoursurge-toolchain-js .     #Build the docker image, supposing the user is in the js directory
cd ../
docker run -v <ABSOLUTE_PATH_TO_PROJECT>:/src widgetparcoursurge-toolchain-js #Creates a docker container and starts the compilation. This command mounts the root of this project to target /src on the docker container
```
    - You can also rely on the "launchGitlabDocker.sh" shell script to pull and launch a pre-compiled docker image automatically.
- Set the variable BUILD_NPM and DEPEND_ON_PARCOURSPROG to False in setup.py
- Follow the installation commands as described above depending on your use case (development or build mode). Remember to use the widgetparcoursurge-toolchain-js docker image instead of the npm command to rebuild the js component of this project at each modification in case you installed those plugins in a development mode.
