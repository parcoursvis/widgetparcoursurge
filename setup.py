from setuptools import setup
from setuptools.command.build_py import build_py
from pathlib import Path
from jupyter_packaging import (
    wrap_installers,
    npm_builder,
    get_data_files,
)
import subprocess
import atexit

BUILD_NPM              = True
DEPEND_ON_PARCOURSPROG = True
DIR    = Path(__file__).resolve().parent
JS_DIR = DIR / 'js'

def get_file_handler(data_files_spec, exclude=None):
    """Get a package_data and data_files handler command."""
    class FileHandler(build_py):
        def run(self):
            self.distribution.package_data = {}
            self.distribution.data_files = get_data_files(data_files_spec, exclude=exclude)
            self.distribution.get_command_obj("sdist").finalize_options()
            build_py.run(self)
    return FileHandler

# Representative files that should exist after a successful build
jstargets = [JS_DIR / 'dist' / 'index.js']

data_files_spec = [
    ('share/jupyter/nbextensions/jsParcoursUrge', 'pyParcoursUrge/nbextension', '*.*'),
    ('share/jupyter/labextensions/jsParcoursUrge', 'pyParcoursUrge/labextension', '**'),
    ('share/jupyter/labextensions/jsParcoursUrge', '.', 'install.json'),
    ('etc/jupyter/nbconfig/notebook.d', '.', 'jsParcoursUrge.json'),
]

dev_builder  = None
dist_builder = None
if BUILD_NPM:
    dev_builder  = npm_builder(path=JS_DIR, npm=['npm'], build_cmd='build');
    dist_builder = npm_builder(path=JS_DIR, npm=['npm'], build_cmd='build:prod');

cmdclass     = wrap_installers(pre_develop=dev_builder, pre_dist=dist_builder, ensured_targets=jstargets)
cmdclass['build_py'] = get_file_handler(data_files_spec)

install_requires=["ipywidgets>=7.6.0,<9"]
if DEPEND_ON_PARCOURSPROG:
    install_requires.append(f"parcoursprog @ file://{DIR}/parcoursurge/back/")

# See setup.cfg for other parameters
setup(cmdclass=cmdclass, install_requires=install_requires)
