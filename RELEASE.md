# Release

We describe here how to release changes on official repositories for the WidgetParcoursUrge project. For the moment we do not plan to release changes on those official repositories: This file is only used for future references.

## Javascript release

To release a new version of jsParcoursUrge on NPM, first register for an NPM account [here](https://www.npmjs.com/), then log in with `npm login`. Then:

1. Update `js/package.json` with the new npm package version
2. Build and publish the npm package inside the `js/` directory:

   ```
   cd js/
   npm install
   npm publish
   cd ..
   ```

## Python release

To release a new version of pyParcoursUrge on PyPI, first make sure that the `build` package is installed: `pip install build`.
Then, make sure that the [[parcoursprog]](https://gitlab.inria.fr/parcoursvis/parcoursurge) Python--C/C++ binding is installed.

1. Update `pyParcoursUrge/_version.py`:
   - Update `__version__`
   - Update `NPM_PACKAGE_RANGE` if necessary

2. Commit changes to `_version.py` and tag the release
   ```
   git add pyParcoursUrge/_version.py
   git tag -a X.X.X -m 'comment'
   ```

3. Generate Python packages and upload to PyPI:
   ```
   python -m build
   twine check dist/*
   twine upload dist/*
   ```

4. Update `_version.py` (add 'dev' and increment minor)
   ```
   git commit -a -m 'Back to dev'
   git push
   git push --tags
