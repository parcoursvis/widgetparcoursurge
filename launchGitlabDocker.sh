echo "Download the docker images from gitlab..."
docker-compose -f docker-compose-gitlab.yml pull

echo "Launch the corresponding docker containers..."
docker-compose -f docker-compose-gitlab.yml up
